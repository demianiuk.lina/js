let qs = JSON.parse(localStorage.getItem('questions'));
const questionMain = document.querySelector('.main-hide');
const skipButtom = document.querySelector('.skipbtn');
const questionContent = document.querySelector('.question');
const answerContent = document.querySelector('.container-answer');
const nowPrize = document.querySelector('.results');
const willPrize = document.querySelector('.will-results');
const message = document.querySelector('.message-hide ');
const skiping = document.querySelector('#skiping')
const elem = document.querySelector('#elem')

let availableQuestion = [];

let availableContent = [];
let current;
let prize = 0;
let will = 100;
let max = 1638400;
let double = 2;

nowPrize.innerHTML = 'Total prize:  ' + prize;
willPrize.innerHTML = 'Prize on current round:  ' + will;

elem.onclick = function () {
  questionMain.classList.toggle('main-hide');
  message.classList.toggle('message-hide');
  message.innerHTML = ' ';
};

const setAvailableQuestion = function () {
  const questionsTotal = qs.length;
  for (let i = 0; i < questionsTotal; i++) {
    availableQuestion.push(qs[i]);
  }
};

const finishGame = function (text, score) {
  message.innerHTML = text + score;
  prize = 0;
  will = 100;
  nowPrize.innerHTML = 'Total prize:' + prize;
  willPrize.innerHTML = 'Prize on current round:' + will;
};

const newQuestion = function () {
  const indexQuestion =
    availableQuestion[Math.floor(Math.random() * availableQuestion.length)];
  current = indexQuestion;
  questionContent.innerHTML = current.question;
  const index1 = availableQuestion.indexOf(indexQuestion);
  availableQuestion.splice(index1, 1);
  const contentLength = current.content.length;
  for (let i = 0; i < contentLength; i++) {
    availableContent.push(i);
  }
  for (let i = 0; i < contentLength; i++) {
    const contents = document.createElement('button');
    contents.innerHTML = current.content[i];
    contents.id = i;
    contents.className = 'contents';
    availableContent.splice(contents, 1);
    answerContent.appendChild(contents);
    contents.setAttribute('onclick', 'result(this)');
  }
};

const result = function (element) {
  const id = parseInt(element.id);
  if (id === current.correct) {
    nowPrize.innerHTML = 'Total prize:' + (prize = +will);
    if (prize === max) {
      questionMain.classList.add('main-hide');
      
      message.classList.toggle('message-hide');
      finishGame('Congratulations! You won 1000000', prize);
    }
    willPrize.innerHTML = 'Prize on current round:' + (will = will * double);
    answerContent.innerHTML = '';
    newQuestion();
  } else{
    questionMain.classList.add('main-hide');
    message.classList.remove('message-hide');
    finishGame('Game over. Your score: ', prize);
    
  }
};

skiping.onclick = function () {
  answerContent.innerHTML = '';
  newQuestion();
  skipButtom.style.display = 'none';
};

setAvailableQuestion();
newQuestion();
result();

