let fix=2;
let max=100;
let amount = prompt('Amount of batteries:');
let num = Number(amount)


if (num >= 0 && Number.isInteger(num)) {
    let rate = prompt('Defective rate: ');
    let ratenum = Number(rate)
    if (ratenum >= 0 && ratenum <= max) {
        let defective = num * (ratenum / max);
        let work = num - defective;
        alert(`Amount of batteries:${num}
Defective rate: ${ratenum} %
Amount of defective batteries: ${defective.toFixed(fix)}
Amount of working batteries:  ${work.toFixed(fix)}`)
    }else {
        alert('Invalid input data')
    }
} else {
    alert('Invalid input data')
}
