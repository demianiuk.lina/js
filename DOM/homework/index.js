/* START TASK 1: Your code goes here */
const cell = document.getElementsByClassName('cell')
const blue = document.getElementsByClassName('blue')
const green = document.getElementsByClassName('special')

for (let element of cell) {
  element.addEventListener('click', addYellow, false);
}

function addYellow(e) {
  let cell = e.target
  cell.classList.add('yellow')
}


for (let element of blue) {
  element.addEventListener('click', addBlue, false);
}

function addBlue(e) {
  let target = e.target;
  let parent = target.parentElement;
  parent.classList.add('row')
}


for (let element of green) {
  element.addEventListener('click', addGreen, false);
}

function addGreen(e) {
  let target = e.target;
  let parent = target.parentElement;
  let table = parent.parentElement;
  let main = table.parentElement;
  main.classList.add('green')
}

/* START TASK 2: Your code goes here */
const input = document.getElementById('tel')
const btn = document.querySelector('[type=button]');
const label = document.querySelector('.label')
const tel = document.querySelector('.tel')

input.addEventListener('input', checkValid, false);

function checkValid() {
  let reg = /^\+380[0-9]{9}$/
  let valid = reg.test(input.value)

  if (valid) {
    btn.removeAttribute('disabled');
    label.innerHTML = ' '
    label.innerHTML = 'Data was successfully sent'
    label.classList.remove('hi')
    tel.classList.remove('invalid')
    label.classList.add('right')
  } else {
    label.classList.remove('right')
    btn.setAttribute('disabled', true);
    label.innerHTML = ''
    label.innerHTML = 'Type number does not follow format +380*********'
    label.classList.add('hi')
    tel.classList.add('invalid')
  }
}


/* START TASK 3: Your code goes here */

let count1 = 0;
let count2 = 0;
const score1 = document.querySelector('.score1');
const goala = document.querySelector('.goal1')
const score2 = document.querySelector('.score2')
const goalb = document.querySelector('.goal2')
const field=document.querySelector('#field')
const ball=document.querySelector('#ball')

field.onclick = function (event) {
  let fieldCoords = this.getBoundingClientRect();

  let ballCoords = {
    top: event.clientY - fieldCoords.top - field.clientTop - ball.clientHeight / 2,
    left: event.clientX - fieldCoords.left - field.clientLeft - ball.clientWidth / 2
  };

  if (ballCoords.top < 0){
    ballCoords.top = 0;
  } 

  if (ballCoords.left < 0){
    ballCoords.left = 0;
  } 

  if (ballCoords.left + ball.clientWidth > field.clientWidth) {
    ballCoords.left = field.clientWidth - ball.clientWidth;
  }

  if (ballCoords.top + ball.clientHeight > field.clientHeight) {
    ballCoords.top = field.clientHeight - ball.clientHeight;
  }

  ball.style.left = ballCoords.left + 'px';
  ball.style.top = ballCoords.top + 'px';

  if (ballCoords.left <= 115 && ballCoords.left >= 85 && ballCoords.top <= 155 && ballCoords.top >= 125) {
    count1 = count1 + 1;
    showScore(count1,goala,score1)
  }
  if (ballCoords.left <= 475 && ballCoords.left >= 445 && ballCoords.top <= 155 && ballCoords.top >= 125) {
    count2 = count2 + 1;
    showScore(count2,goalb,score2)
  }

}

function showScore(count,goal,score){
  score.innerHTML = count
    goal.style.display = 'block'
    setTimeout(function () {
      goal.style.display = 'none';
    }, 3000);
}

