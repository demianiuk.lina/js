const isEquals = (arg1, arg2) => arg1 === arg2;

const numberToString = (number) => String(number);

const storeNames = (...arg) => Array.from(arg);

const getDivision = (numb1, numb2) => numb1 > numb2 ? numb1 / numb2 : numb2 / numb1;

function negativeCount(array) {
    let count = 0;
    array.forEach(function (element) {
        element < 0 ? count = count + 1 : count
    });
    return count;
}

const letterCount = (string, char) => string.split(char).length - 1;
console.log(letterCount('Barny', 'y'))

function countPoints(score) {
    let count = 0;
    let win=3;
    score.forEach(function (element) {
        let a = element.split(':')
        if (Number(a[0]) > Number(a[1])) {
            count = count + win;
        } else if (Number(a[0]) === Number(a[1])) {
            count = count + 1;
        }
    });
    return count;
}

isEquals();
numberToString();
storeNames();
getDivision();
negativeCount();
letterCount();
countPoints();