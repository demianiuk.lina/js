
$(function () {
  const $list = $(".list");
  const $input = $("#add-input");
  const $add = $("#add-submit");

  const todos = [
   
  ];

  $(document).ready(function () {
    function showToDoList() {

      let newArr = JSON.parse(localStorage.getItem("todoList"));
      todos.push(newArr)
      
      for (let i = 0; i < newArr.length; i++) {

        let name = newArr[i].text;
        let done = newArr[i].done;
        if (name != undefined) {
          if (done === false) {
            $($list).append(
              "<li class='item'><span class='item-text'>" +
              name +
              "</span>" +
              '<button class="item-remove">Remove' +
              "</button>" +
              "</li>"
            );
          } else {
            $($list).append(
              "<li class='item'><span class='item-text done'>" +
              name +
              "</span>" +
              '<button class="item-remove">Remove' +
              "</button>" +
              "</li>"
            );
          }
        }

      }
    }

    

    $($add).click(function (event) {
      event.preventDefault();
      if (!$input.val()) {
        return false;
      } else {
        $($list).append(
          "<li class='item'><span class='item-text'>" +
          $input.val() +
          "</span>" +
          '<button class="item-remove">Remove' +
          "</button>" +
          "</li>"
        );
        let newObj = new Object();
        newObj.text = $input.val();
        newObj.done = false;
        todos.push(newObj);
        $input.val("");
      }
      localStorage.setItem("todoList", JSON.stringify(todos));
      
    });

    $(document).on("click", ".item-remove", function () {
      let $parent = $(this).parent();
      let $sister = $(this).prev();
      console.log($sister.html());
      for (let i = 0; i < todos.length; i++) {
        let index = i;
        if (todos[i].text === $sister.html()) {
          todos.splice(index, 1);
        }
      }
      $parent.remove();
      localStorage.setItem("todoList", JSON.stringify(todos));
      
    });

    $(document).on("click", ".item-text", function () {
      $(this).addClass("done");
      for (let i = 0; i < todos.length; i++) {
        if (todos[i].text === $(this).html()) {
          todos[i].done = true;
        }
      }
      localStorage.setItem("todoList", JSON.stringify(todos));
      
    });
    showToDoList();

   
  });
});

