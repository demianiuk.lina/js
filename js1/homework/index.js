//Task1

function convert(list) {
    let arr = [];
    for (let i = 0; i < list.length; i++) {
        if (typeof list[i] === 'string') {
            arr[i] = Number(list[i])
        } else {
            arr[i] = String(list[i])
        }
    }
    return arr;
}

// console.log(convert([1, 2, 3, 4]))

//Task2

function executeforEach(list, func) {
    for (let i = 0; i < list.length; i++) {
        list[i] = func(list[i])
    }
}

// executeforEach([1,2,3], function(el) {console.log(el * 2)}) 

//Task3

function mapArray(list, func) {
    let arrMap = [];
    executeforEach(list, function (el) {
        if (typeof el === 'string') {
            el = Number(el);
        }
        arrMap.push(func(el))
    });
    return arrMap;
}
// console.log(mapArray(['2', 5, 8], function (el) {
//     return el + 3;
// }));

//Task4

function filterArray(list, func) {
    let arrFilt = [];
    executeforEach(list, function (el) {

        if (func(el) === true) {
            arrFilt.push(el)

        }

    });
    return arrFilt;
}

// console.log(filterArray([2, 5, 8], function (el) {
//     return el % 2 === 0
// }))

//Task5

function getValuePosition(list, arg) {
    let count = 0;
    for (let i = 0; i < list.length; i++) {
        if (list[i] === arg) {
            count = count + 1
            return count
        } else if (list.length === count + 1) {
            return false
        } else {
            count = count + 1
        }

    }


}

// console.log(getValuePosition([2, 5, 8], 8))

//Task6

function flipOver(list) {
    let string = '';
    for (let i = list.length - 1; i >= 0; i--) {
        string += list[i]

    }
    return string
}

// console.log(flipOver('hey world'))

//Task7

function makeListFromRange(list) {
    let arRange = [];
    for (let i = 0; i < list[1] - list[0] + 1; i++) {
        arRange[i] = list[0] + i
    }
    return arRange
}

// console.log(makeListFromRange([2, 7]))

//Task8

function getArrayOfKeys(obj, keys) {
    let arrKeys = []
    executeforEach(obj, function (el) {
        for (let key in el) {
            if (key === keys) {
                arrKeys.push(el[key])
            }
        }
    });
    return arrKeys
}

const fruits = [
    { name: 'apple', weight: 0.5 },
    { name: 'pineapple', weight: 2 }
];

// console.log(getArrayOfKeys(fruits, 'name'))

//Task9

function getTotalWeight(obj) {
    let count = 0
    executeforEach(obj, function (el) {
        for (let key in el) {
            if (key === 'weight') {
                count = count + el[key]
            }
        }
    });
    return count
}

const basket = [
    { name: 'Bread', weight: 0.3 },
    { name: 'Coca-Cola', weight: 0.5 },
    { name: 'Watermelon', weight: 8 }
];

// console.log(getTotalWeight(basket))

//Task10

function getPastDay(date, days) {
    let copy = new Date(date);
    copy.setDate(date.getDate() - days);
    let dateNumber = copy.getDate();
    return dateNumber;
}

// const date = new Date(2020, 01, 2);
// console.log(getPastDay(date, 1));
// console.log(getPastDay(date, 2));
// console.log(getPastDay(date, 365));

//Task11

function formatDate(date) {
    let copy;
    let ten=10;
    if (date === ' ') {
        copy = new Date();
    } else {
        copy = new Date(date);
    }
    let yy = copy.getFullYear();
    let dd = copy.getDate();
    if (dd < ten){
        dd = '0' + dd;
    }
    let mm = copy.getMonth() + 1;
    if (mm < ten){
        mm = '0' + mm;
    }
    let hh = copy.getHours();
    if (hh < ten){
        hh = '0' + hh;
    }
    let mmm = copy.getMinutes();
    if (mmm < ten){
        mmm = '0' + mmm;
    }
    return yy + '/' + mm + '/' + dd + ' ' + hh + ':' + mmm;

}

// console.log(formatDate(new Date('6/15/2019 09:15:00')))
// console.log(formatDate(new Date()))
