
import { randomSelection } from './randomSelection.js'
import { count } from './winner.js'

const round = document.querySelector('.round')
const result = document.querySelector('.result')
const butn = document.querySelector('.reser')
let youscore = 0;
let compscore = 0;

export const makeSelection = (selection) => {
    const computerSelection = randomSelection()
    let p = document.createElement('p')
    let win = document.createElement('strong')

    p.innerHTML = `Round ${count}, ${selection.name} vs ${computerSelection.name}`;
    round.append(p)
    if (computerSelection.name === selection.beats) {
        youscore = youscore + 1;
        win.innerHTML = 'Yo\'ve WON!'
        p.className = 'won'
        p.append(win)
    }
    if (selection.name === computerSelection.beats) {
        compscore = compscore + 1;
        win.innerHTML = 'Yo\'ve LOST!'
        p.className = 'lost'
        p.append(win)
    } else if (selection.name === computerSelection.name) {
        win.innerHTML = 'Dead heat';
        p.className = 'dead'
        p.append(win)
    }

    if (count === 3) {
        let winner = document.createElement('h2')
        if (youscore > compscore) {
            winner.innerHTML = 'You\'ve winner'
            result.append(winner)
        } else if (youscore < compscore) {
            winner.innerHTML = 'Computer\'s winner'
            result.append(winner)
        } else {
            winner.innerHTML = 'Dead heat'
            result.append(winner)
        }

    }

}

butn.addEventListener('click', function () {
    count = 0;
    youscore = 0;
    compscore = 0;
    result.innerHTML = ''
    round.innerHTML = ''
})