const game=[
    {
        name: 'Rock',
        beats: 'Scissors',
    },
    {
        name: 'Scissors',
        beats: 'Paper',
    },
    {
        name: 'Paper',
        beats: 'Rock',
    },
];

localStorage.setItem('game', JSON.stringify(game));