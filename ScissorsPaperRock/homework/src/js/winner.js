import { makeSelection } from './makeSelection'

let game = JSON.parse(localStorage.getItem('game'));
const selectionButtons = document.querySelectorAll('[data-selection]')
export let count = 0;



selectionButtons.forEach(selectionButton => {
    selectionButton.addEventListener('click', function () {
        count = count + 1
        if (count <= 3) {
            const selectionName = selectionButton.dataset.selection
            const selection = game.find(selection => selection.name === selectionName)
            makeSelection(selection)
        }

    })
})




