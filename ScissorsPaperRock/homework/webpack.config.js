const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: ['./src/app.js',
  './src/scss/style.scss'
],

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
        test: /\.(css|scss)$/,
        use: [               
            {
                loader: MiniCssExtractPlugin.loader,
                options: {
                    publicPath: './src/scss',
                    
                }
            },
            { loader: "css-loader" },
            { loader: "sass-loader" }
        ],
  
    },{
      test: /\.js$/, 
    exclude: /node_modules/, 
    use: ["babel-loader"], 
    }]
},

    
  
plugins: [
    new MiniCssExtractPlugin({
        filename: 'style.css'
    })
]

};


